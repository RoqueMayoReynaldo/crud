﻿

$(document).ready(function () {



    var request = $.ajax(
        {
            url: '/Home/ListaPersonas',
            type: 'post'

        }

    );
    request.done(
        function (respuesta) {
            $("tbody").html(respuesta);

            var $btnModal = $(".editar");
            $btnModal.click(

                function (e) {
                    e.preventDefault();



                    var request = $.ajax(

                        {

                            url: 'AdminUsuario/EditarForm?id=' + $(this).val(),
                            type: 'get',

                        }
                    );

                    request.done(
                        function (respuesta) {
                            $("body").append(respuesta);
                            var modalForm = $(".modal");
                            modalForm.modal('show');
                            $(".cancelarModal").click(

                                function () {



                                    modalForm.remove();
                                    $(".modal-backdrop").remove();
                                }

                            );
                            $(".confirmarModal").click(

                                function () {

                                    var idP = $(this).val();

                                    var request = $.ajax(

                                        {
                                            url: '/AdminUsuario/Editar',
                                            type: 'post',
                                            data: {
                                                id: idP,
                                                nombre: $('#editNombre-' + idP).val(),
                                                descripcion: $('#editDesc-' + idP).val()
                                            }
                                        }
                                    );
                                    request.done(
                                        function (person) {
                                            

                                            $("#celdaNombre-" + idP).html(person.nombre);
                                            $("#celdaDescripcion-" + idP).html(person.descripcion);
                                            modalForm.remove();
                                            $(".modal-backdrop").remove();

                                        }


                                    );


                                }

                            );


                        }


                    );
                }

            );



            var $btnDelete = $(".eliminar");

            $btnDelete.click(
                function () {


                    var idPersona = $(this).val();

                  
                    var request = $.ajax(
                        
                        {
                            url: '/AdminUsuario/Eliminar?id=' + idPersona,
                            type: 'get'

                        }
                    );
                    request.done(

                        function () {
                           

                            $("#fila-" + idPersona).remove();

                            alert("Persona con id: "+idPersona +" eliminada");
                        }
                        
                    );
                    request.fail(
                        
                        function () {
                            alert("Falla al ejecutar el request");
                        }

                    );
                }

            );

        });






}

);

