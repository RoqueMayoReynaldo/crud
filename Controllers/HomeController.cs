﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Prueba.DB;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Controllers
{
    public class HomeController : Controller
    {
        private AppPruebaContext context;
        public HomeController(AppPruebaContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<Persona> personas = context.Personas.ToList();

            return View();
        }


        [HttpPost]
        public ViewResult ListaPersonas()
        {

            List<Persona> personas = context.Personas.ToList();

            return View(personas);
        }

       

    }
}
