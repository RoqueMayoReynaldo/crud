﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Prueba.DB;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Controllers
{
    public class AdminUsuarioController : Controller
    {

        private AppPruebaContext context;
        public AdminUsuarioController(AppPruebaContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public ViewResult EditarForm(int id)
        {
            Persona persona = context.Personas.FirstOrDefault(o=>o.id==id);
            

            return View(persona);
        }
        [HttpPost]
        public Persona Editar(Persona p)
        {

            Persona persona = context.Personas.Find(p.id);
            if (!string.IsNullOrEmpty(p.nombre))
                persona.nombre = p.nombre;

            if (!string.IsNullOrEmpty(p.descripcion))
                persona.descripcion = p.descripcion;


            context.SaveChanges();
            return persona;

        }
        [HttpGet]
        public void Eliminar(int id)
        {
            Persona persona = context.Personas.Find(id);
            context.Personas.Remove(persona);
            context.SaveChanges();


        }

        [HttpGet]
        public IActionResult Crear()
        {


            return View();
        }
        [HttpPost]
        public IActionResult Crear(Persona persona)
        {
            if (ModelState.IsValid)
            {
                context.Personas.Add(persona);
                context.SaveChanges();
               
            }
            else
            {
                return View("Crear");
            }

            return RedirectToAction("Index","Home") ;
        }
    }
}
