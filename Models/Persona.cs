﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Models
{
    public class Persona
    {
        public int id { get; set; }
        [Required(ErrorMessage ="Nombre de la persona requerido")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Descripcion de la persona requerida")]
        [StringLength(100, MinimumLength = 5,ErrorMessage ="5 caracteres como minimo y 100 como maximo")]
        public string descripcion { get; set; }
    }
}
