﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.DB.Mapping
{
    public class PersonaMap : IEntityTypeConfiguration<Persona>

    {
  

        public void Configure(EntityTypeBuilder<Persona> builder)
        {

            builder.ToTable("Persona", "dbo");
            builder.HasKey(Persona => Persona.id);
        }
    }
}
