﻿using Microsoft.EntityFrameworkCore;
using Prueba.DB.Mapping;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.DB
{
    public class AppPruebaContext : DbContext
    {
         public DbSet<Persona> Personas { get; set; }

        public AppPruebaContext(DbContextOptions<AppPruebaContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
             modelBuilder.ApplyConfiguration(new PersonaMap());


        }


    }
}